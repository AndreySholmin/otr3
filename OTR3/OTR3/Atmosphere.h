#pragma once
#include <math.h>
#include <iostream>

class Atmosphere {
private:
	//const double a_c = 340.294;      /// �������� �����
	//const double RO_c = 1.225;       /// ���������

	const double g_c = 9.80665;        /// ��������� ���������� �������
	const double p_c = 101325;         /// ��������
	const double T_c = 288.15;         /// ����������� ��������
	const double R_c = 6356767;        /// ������
	const double K = 1.4;              /// ���������� ��������
	const double R = 287.05287;        /// �������� ������� �����������

	const double arrBetta[10]{ -0.0065, -0.0065, -0.0065, 0.0,    0.001,  0.0028, 0.0,   -0.0028, -0.0020, 0.0 };
	const double arrH[12]{ -2000,        0.0,     11000,  20000,  32000,  47000,  51000,  71000,   85000,  94000, 102450, 117777 };
	//const double arrh[12]{ -1999,        0.0,     11019,  20063,  32162,  47350,  51412,  71802,   86152,  95411, 104128, 120000 };
	const double arrT[12]{ 301.15,       288.15,  216.65, 216.65, 228.65, 270.65, 270.65, 214.65,  186.65 };

	double geoHeight;                        /// ���������������� ������
	double arrp[12]{ 0, p_c, 0 };    /// ������ 

	double T;                     /// ����������������� �����������, �� 94 ��
	double p;                     /// ��������
	double ro;                    /// ���������
	double a;                     /// �������� �����
	double g;                     /// ��������� ���������� �������
	double height;                /// �������������� ������

	inline void heightToSetGeoHeight();
	inline void temperature();
	inline void pressure(const double h);
	inline void density();
	inline void soundVelocity();
	inline void accelerationGravity();

public:
	Atmosphere(const double h);

	void updateParameters(const double h);

	void setHeight(const double height);

	double getTemperature();
	double getPressure();
	double getDensity();
	double getSoundVelocity();
	double getAccelerationGravity();

};
