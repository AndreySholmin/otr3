#pragma once
#include "Vehicle.h"

class MathModel {
private:
	// for gravity force 
	const double R = 6371E3;
	const double mu = 3.986004415E14;
	double Gy = NULL;

public:

	/// patameters RG 
	double RGro = NULL;   double dRGro = NULL;
	double RGly = NULL;   double dRGly = NULL;
	double RGmy = NULL;   double dRGmy = NULL;
	double RGny = NULL;   double dRGny = NULL;

	double dOmegaX = NULL, dOmegaY = NULL, dOmegaZ = NULL;
	double dX = NULL, dY = NULL, dZ = NULL;
	double dVxg = NULL, dVyg = NULL, dVzg = NULL;

	/// the transition matrix between the coordinate systems
	double a[3][3];

	void setRGParameters(double pitch, double yaw, double roll);

	double calcGetPitch();
	double calcGetYaw();
	double calcGetRoll();

	void setMatrixRGParameters();

	void updateDerivatives(Vehicle& la);

};