#pragma once

#include <iomanip> 
#include <fstream>
#include <string>

#include "OTR.h"
#include "MathModel.h"
#include "Atmosphere.h"
#include "const.h"

/// <summary>
/// ������ flightSimple � flightForAreaImpact ���������, �� � ����� ���������������� ��� ���� 
/// ���� ������ ��������� ��������.
/// ����� ��� ��������� ������ �� �������, ����������� ��������� �������.
/// </summary>
class Integration {
public:

	double flightSimple(const int typeDelay, const int TdelayRange, const double dh, const double xTarget, const double zTarget, std::string nameParamFlight, long STEP0);

	double flightForAreaImpact(const int typeDelay, const int TdelayRange, const double dh, const double xTarget, const double zTarget);



	void printParametersFlight(Vehicle& la, MathModel& mm, std::ofstream& file);

	void printParametersFlight(std::ofstream& file);

};
