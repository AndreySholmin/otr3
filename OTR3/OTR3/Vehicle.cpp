#include "Vehicle.h"

void Vehicle::updateParameters(const double density, const double soundVelocity) {
	
	r = sqrt((xtarget - x) * (xtarget - x) + (ytarget - y) * (ytarget - y) + (ztarget - z) * (ztarget - z));

	velocity = sqrt((Vx - Wx) * (Vx - Wx) + (Vy - Wy) * (Vy - Wy) + (Vz - Wz) * (Vz - Wz));

	betta = asin((Vz - Wz) / velocity);
	alpha = -atan2((Vy - Wy), (Vx - Wx));

	q = 0.5 * density * velocity * velocity;
	mach = velocity / soundVelocity;

	qSm = q * Sm;
	qSmL = qSm * LENGTH;
	qSmLL_v = qSmL * LENGTH / velocity;
}
