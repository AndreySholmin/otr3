#include "Atmosphere.h"


void Atmosphere::setHeight(const double height) {
	this->height = height;
}

/// ����������� ��� ������� ����� ��������
Atmosphere::Atmosphere(const double height) {
	// ����������� ������ ����� �������, ��� ������ ���������� ������� ���� ���, ������� ����� �� �� ������� 
	// �� ������� ���������, � ������������ ����������� ���� �������� � �������� ������ �� ��� ��� ������ 94 ��
	if (height < 94E3) {
		setHeight(height);
		heightToSetGeoHeight();
	}
	else {
		std::cout << "\nERROR!!!\n������ ������ 94 ��!!!\n";
		std::system("pause");
	}
	// ��������������� ������ ����� �� 94 ��
	// ������ ����� // �������� ������a ��������
	for (int i = 2; i < 10; i++) {
		pressure(arrH[i]);
		arrp[i] = p;
	}
}

/// �����, ������������ ������ ���������� ��������� � ����������� �� �������������� ������
void Atmosphere::updateParameters(const double height) {
	setHeight(height);
	heightToSetGeoHeight();
	temperature();
	pressure(geoHeight);
	density();
	soundVelocity();
	//accelerationGravity();
}

/// ������� �� �������������� ������ � ����������������
inline void Atmosphere::heightToSetGeoHeight() {
	geoHeight = (R_c * height / (R_c + height));
}
/// ���������� �����������
inline void Atmosphere::temperature() {
	
	for (int i = 1; i < 10; i++) {
		if (geoHeight > arrH[i] && geoHeight < arrH[i + 1]) {
			
			T = arrT[i] + arrBetta[i + 1] * (geoHeight - arrH[i]);
			break;

		} else if (geoHeight == arrH[i]) {
			
			T = arrT[i];
			break;

		}
	}

	if (geoHeight > arrH[0] && geoHeight < arrH[1]) {
		T = arrT[1] + arrBetta[1] * (geoHeight - arrH[1]);
	}
}

/// ���������� ��������
inline void Atmosphere::pressure(const double geoHeight) {
	if (geoHeight < 0) {
	
		p = arrp[1] * pow((1 + (arrBetta[1]) * (geoHeight - arrH[1]) / arrT[1]), (-g_c / (R * arrBetta[1])));
	
	} else if (geoHeight == 0) {
		
		p = p_c;
	
	} else if (geoHeight > 0) {
		for (int i = 1; i < 12; i++) {
			if (geoHeight > arrH[i] && geoHeight <= arrH[i + 1]) {
				if (arrBetta[i + 1] == 0) {
				
					p = arrp[i] * exp(g_c * (arrH[i] - geoHeight) / (R * arrT[i]));	break;
				
				} else {
				
					p = arrp[i] * pow((1 + (arrBetta[i + 1]) * (geoHeight - arrH[i]) / arrT[i]), (-g_c / (R * arrBetta[i + 1]))); break;
				
				}
			}
		}
	}
}

/// ���������� ���������
inline void Atmosphere::density() {
	ro = p / (R * T);
}

/// ���������� �������� �����
inline void Atmosphere::soundVelocity() {
	a = sqrt(K * R * T);
}

/// ���������� ��������� ���������� �������
inline void Atmosphere::accelerationGravity() {
	g = g_c * (R_c / (R_c + height)) * (R_c / (R_c + height));
}

/// �������
double Atmosphere::getTemperature() {
	return T;
}

double Atmosphere::getPressure() {
	return p;
}

double Atmosphere::getDensity() {
	return ro;
}

double Atmosphere::getSoundVelocity() {
	return a;
}

double Atmosphere::getAccelerationGravity() {
	return g;
}
