#pragma once

#include <iostream>
#include "Vehicle.h"
#include "const.h"

class Control {
private:
	// ���������� ��������� ��
	const double K_AD_development = 1;

	const double Kcc_p = 0.95, Ky_p = 1, Krp_p = 1, Kdg_p = 1, Ecc_p = 0.35;
	const double Kcc_y = 0.95, Ky_y = 1, Krp_y = 1, Kdg_y = 1, Ecc_y = 0.35;
	const double Ecc_r = 0.35, Tcc_r = 0.01;

	double �riticalDeltaAngle = 15 * DEGR_BY_RAD;

	double Kkp = 7, Kdfi = 7, Kky = 7, Kdhi = 7;

	double d1 = NULL, d2 = NULL, d3 = NULL;

	// for 1 algoritm implementation delay
	double* deltaPitchArray;
	double* deltaYawArray;
	double* deltaRollArray;

public:

	int indexTemp = -1;
	int TdelayRange = -1;
	int typeDelay = -1;

	void updateParameters(Vehicle& vh);

	void setTypeDelay(int typeDelay);
	void setTdelayRange(int typeDelay);

	int getTypeDelay();
	int getTdelayRange();

	~Control();

};