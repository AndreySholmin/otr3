#include "Integration.h"

double Integration::flightSimple(
	const int typeDelay, const int TdelayRange, const double dh, 
	const double xTarget, const double zTarget, std::string nameParamFlight, long STEP0
) {

	OTR otr;

	otr.cs.setTypeDelay(typeDelay);
	otr.cs.setTdelayRange(TdelayRange);

	otr.vh.ytarget = 0;
	otr.vh.xtarget = xTarget;
	otr.vh.ztarget = zTarget;

	double dt = dh;

	double Wxg = 0;
	double Wyg = 0;
	double Wzg = 0;

	// print to file
	long step = 0;
	const long STEP = STEP0;

	MathModel mm;
	Atmosphere sa(0);

	std::ofstream FileParamFlight(nameParamFlight, std::ios::out);
	printParametersFlight(FileParamFlight);

	// Calculation
	mm.setRGParameters(otr.vh.pitch, otr.vh.yaw, otr.vh.roll);
	mm.setMatrixRGParameters();

	otr.vh.Vxg = mm.a[0][0] * otr.vh.Vx + mm.a[1][0] * otr.vh.Vy + mm.a[2][0] * otr.vh.Vz;
	otr.vh.Vyg = mm.a[0][1] * otr.vh.Vx + mm.a[1][1] * otr.vh.Vy + mm.a[2][1] * otr.vh.Vz;
	otr.vh.Vzg = mm.a[0][2] * otr.vh.Vx + mm.a[1][2] * otr.vh.Vy + mm.a[2][2] * otr.vh.Vz;

	//������� �������� ����� �� �� � ����
	otr.vh.Wx = mm.a[0][0] * Wxg + mm.a[0][1] * Wyg + mm.a[0][2] * Wzg;
	otr.vh.Wy = mm.a[1][0] * Wxg + mm.a[1][1] * Wyg + mm.a[1][2] * Wzg;
	otr.vh.Wz = mm.a[2][0] * Wxg + mm.a[2][1] * Wyg + mm.a[2][2] * Wzg;

	while (otr.vh.y >= 1E-4) {

		sa.updateParameters(otr.vh.y);

		otr.updateParameters(sa);

		mm.updateDerivatives(otr.vh);

		// Print parameters flight in file
		if (step % STEP == 0) {
			//for debug: cout << "r = " << otr.fp.r << "\tx = " << otr.fp.x << "\ty = " << otr.fp.y << "\tz = " << otr.fp.z << "\tt = " << otr.fp.t << "\t" << otr.fp.deltaPitch << "\t" << otr.fp.deltaYaw << "\t" << otr.fp.deltaRoll << endl;
			printParametersFlight(otr.vh, mm, FileParamFlight);
		}
		step++;

		/// �������������� ������� ������
		otr.vh.Vxg += mm.dVxg * dt;
		otr.vh.Vyg += mm.dVyg * dt;
		otr.vh.Vzg += mm.dVzg * dt;

		otr.vh.x += mm.dX * dt;
		otr.vh.y += mm.dY * dt;
		otr.vh.z += mm.dZ * dt;

		otr.vh.omegax += mm.dOmegaX * dt;
		otr.vh.omegay += mm.dOmegaY * dt;
		otr.vh.omegaz += mm.dOmegaZ * dt;

		mm.RGro += mm.dRGro * dt;
		mm.RGly += mm.dRGly * dt;
		mm.RGmy += mm.dRGmy * dt;
		mm.RGny += mm.dRGny * dt;

		otr.vh.t += dt;

		mm.setMatrixRGParameters();

		otr.vh.pitch = mm.calcGetPitch();
		otr.vh.yaw = mm.calcGetYaw();
		otr.vh.roll = mm.calcGetRoll();

		/// ������� �� ��������� �� � ��������� ��������
		otr.vh.Vx = mm.a[0][0] * otr.vh.Vxg + mm.a[0][1] * otr.vh.Vyg + mm.a[0][2] * otr.vh.Vzg;
		otr.vh.Vy = mm.a[1][0] * otr.vh.Vxg + mm.a[1][1] * otr.vh.Vyg + mm.a[1][2] * otr.vh.Vzg;
		otr.vh.Vz = mm.a[2][0] * otr.vh.Vxg + mm.a[2][1] * otr.vh.Vyg + mm.a[2][2] * otr.vh.Vzg;
	}

	printParametersFlight(otr.vh, mm, FileParamFlight);

	FileParamFlight.close();

	std::cout << "\nxTarget = " << otr.vh.xtarget << "\tzTarget = " << otr.vh.ztarget << std::endl;
	std::cout << "x = " << otr.vh.x << "\tz = " << otr.vh.z << std::endl;
	std::cout << "r = " << otr.vh.r << std::endl;

	return otr.vh.r;
}

double Integration::flightForAreaImpact(const int typeDelay, const int TdelayRange, const double dh, double xTarget, double zTarget) {

	OTR otr;

	otr.cs.setTypeDelay(typeDelay);
	otr.cs.setTdelayRange(TdelayRange);

	otr.vh.ytarget = 0;
	otr.vh.xtarget = xTarget;
	otr.vh.ztarget = zTarget;

	double dt = dh;

	double Wxg = 0;
	double Wyg = 0;
	double Wzg = 0;

	MathModel mm;
	Atmosphere sa(0);

	// Calculation
	mm.setRGParameters(otr.vh.pitch, otr.vh.yaw, otr.vh.roll);
	mm.setMatrixRGParameters();

	otr.vh.Vxg = mm.a[0][0] * otr.vh.Vx + mm.a[1][0] * otr.vh.Vy + mm.a[2][0] * otr.vh.Vz;
	otr.vh.Vyg = mm.a[0][1] * otr.vh.Vx + mm.a[1][1] * otr.vh.Vy + mm.a[2][1] * otr.vh.Vz;
	otr.vh.Vzg = mm.a[0][2] * otr.vh.Vx + mm.a[1][2] * otr.vh.Vy + mm.a[2][2] * otr.vh.Vz;

	//������� �������� ����� �� �� � ����
	otr.vh.Wx = mm.a[0][0] * Wxg + mm.a[0][1] * Wyg + mm.a[0][2] * Wzg;
	otr.vh.Wy = mm.a[1][0] * Wxg + mm.a[1][1] * Wyg + mm.a[1][2] * Wzg;
	otr.vh.Wz = mm.a[2][0] * Wxg + mm.a[2][1] * Wyg + mm.a[2][2] * Wzg;

	while (otr.vh.y >= 1E-4) {

		sa.updateParameters(otr.vh.y);

		otr.updateParameters(sa);

		mm.updateDerivatives(otr.vh);

		/// �������������� ������� ������
		otr.vh.Vxg += mm.dVxg * dt;
		otr.vh.Vyg += mm.dVyg * dt;
		otr.vh.Vzg += mm.dVzg * dt;

		otr.vh.x += mm.dX * dt;
		otr.vh.y += mm.dY * dt;
		otr.vh.z += mm.dZ * dt;

		otr.vh.omegax += mm.dOmegaX * dt;
		otr.vh.omegay += mm.dOmegaY * dt;
		otr.vh.omegaz += mm.dOmegaZ * dt;

		mm.RGro += mm.dRGro * dt;
		mm.RGly += mm.dRGly * dt;
		mm.RGmy += mm.dRGmy * dt;
		mm.RGny += mm.dRGny * dt;

		otr.vh.t += dt;

		mm.setMatrixRGParameters();

		otr.vh.pitch = mm.calcGetPitch();
		otr.vh.yaw = mm.calcGetYaw();
		otr.vh.roll = mm.calcGetRoll();

		/// ������� �� ��������� �� � ��������� ��������
		otr.vh.Vx = mm.a[0][0] * otr.vh.Vxg + mm.a[0][1] * otr.vh.Vyg + mm.a[0][2] * otr.vh.Vzg;
		otr.vh.Vy = mm.a[1][0] * otr.vh.Vxg + mm.a[1][1] * otr.vh.Vyg + mm.a[1][2] * otr.vh.Vzg;
		otr.vh.Vz = mm.a[2][0] * otr.vh.Vxg + mm.a[2][1] * otr.vh.Vyg + mm.a[2][2] * otr.vh.Vzg;
	}


	return otr.vh.r;
}

void Integration::printParametersFlight(std::ofstream& file) {
	file << "t" << "\t"
		<< "r" << "\t"
		<< "x" << "\t" << "y" << "\t" << "z" << "\t"
		<< "V" << "\t" << "Vxg" << "\t" << "Vyg" << "\t" << "Vzg" << "\t"
		<< "omegax" << "\t" << "omegay" << "\t" << "omegaz" << "\t"
		<< "omegax gr " << "\t" << "omegay gr" << "\t" << "omegaz gr" << "\t"
		<< "PitchGr" << "\t" << "YawGr" << "\t" << "RollGr" << "\t"
		<< "DeltaPitch[��]" << "\t" << "DeltaYaw[��]" << "\t" << "DeltaRoll[��]" << "\t"
		<< "Vx" << "\t" << "Vy" << "\t" << "Vz" << "\t"
		<< "alpha" << "\t" << "betta" << "\t" << "derRoll" << "\t" << std::endl;
}

void Integration::printParametersFlight(Vehicle& la, MathModel& mm, std::ofstream& file) {

	file << std::setprecision(3) << la.t << "\t"
		<< std::setprecision(6) << la.r << "\t"
		<< std::setprecision(6) << la.x << "\t"
		<< std::setprecision(6) << la.y << "\t"
		<< std::setprecision(6) << la.z << "\t"
		<< std::setprecision(6) << la.velocity << "\t"
		<< std::setprecision(6) << la.Vxg << "\t"
		<< std::setprecision(6) << la.Vyg << "\t"
		<< std::setprecision(6) << la.Vzg << "\t"
		<< std::setprecision(6) << la.omegax << "\t"
		<< std::setprecision(6) << la.omegay << "\t"
		<< std::setprecision(6) << la.omegaz << "\t"
		<< std::setprecision(6) << la.omegax * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.omegay * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.omegaz * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.pitch * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.yaw * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.roll * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.deltaPitch * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.deltaYaw * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.deltaRoll * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.Vx << "\t"
		<< std::setprecision(6) << la.Vy << "\t"
		<< std::setprecision(6) << la.Vz << "\t"
		<< std::setprecision(6) << la.alpha * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.betta * RAD_BY_DEGR << "\t"
		<< std::setprecision(6) << la.derRoll * RAD_BY_DEGR << "\t" << std::endl;
}
