#include "Aerodynamics.h"

void Aerodynamics::updateParameters(Vehicle& vh) {

	vh.Y_alpha = CyAlpha(vh.mach) * vh.qSm;
	vh.Y_delta_p = CyDeltaV(vh.mach, vh.alpha) * vh.qSm;

	vh.Z_beta = CzBetta(vh.mach) * vh.qSm;
	vh.Z_delta_y = CzDeltaN(vh.mach, vh.betta) * vh.qSm;

	vh.Mx_omgx = mxOmegax() * vh.qSmLL_v;
	//vh.Mx_delta_r = K_AD_development * mxDeltaR() * vh.qSmL;
	vh.Mx_delta_r = mxDeltaR(vh.q, vh.SmL) * vh.qSmL;

	vh.My_omgy = myOmegay(vh.mach) * vh.qSmLL_v;
	vh.My_beta = myBetta(vh.mach) * vh.qSmL;
	vh.My_delta_y = myDeltaY(vh.mach, vh.betta) * vh.qSmL;

	vh.Mz_omgz = mzOmegaz(vh.mach) * vh.qSmLL_v;
	vh.Mz_alpha = mzAlpha(vh.mach) * vh.qSmL;
	vh.Mz_delta_p = mzDeltaP(vh.mach, vh.alpha) * vh.qSmL;

	vh.X = -Cx(vh.mach) * vh.qSm;
	vh.Y = vh.Y_alpha * vh.alpha + vh.Y_delta_p * vh.deltaPitch;
	vh.Z = vh.Z_beta * vh.betta + vh.Z_delta_y * vh.deltaYaw;

	vh.Mx = vh.Mx_omgx * vh.omegax + vh.Mx_delta_r * vh.deltaRoll;
	vh.My = vh.My_omgy * vh.omegay + vh.My_beta * vh.betta + vh.My_delta_y * vh.deltaYaw;
	vh.Mz = vh.Mz_omgz * vh.omegaz + vh.Mz_alpha * vh.alpha + vh.Mz_delta_p * vh.deltaPitch;
}

///////////////////////////
// aerodynamic coefficients
///////////////////////////
inline double Aerodynamics::Cx(const double mach) {
	return (1 / (73.211 / exp(mach) - 47.483 / mach + 16.878));
}

inline double Aerodynamics::CyAlpha(const double mach) {
	const double Ds = 1.86 * (11.554 / exp(mach) - 2.5191E-3 * mach * mach - 5.024 / mach + 52.836E-3 * mach + 4.112);
	if (Ds >= 0) {
		return sqrt(Ds);
	}
	else {
		return 1.93254;       //CyAlpha = (1.86 * 1.039);
	}
}
inline double Aerodynamics::CyDeltaV(const double mach, const double alpha) {
	const double alpha_l = fabs(alpha * RAD_BY_DEGR);

	const double p1 = 1 / (243.84E-3 / exp(alpha_l) + 74.309E-3);
	const double p2 = log10(1.9773 * alpha_l * alpha_l - 25.587 * alpha_l + 83.854);
	const double p3 = 18.985 * alpha_l * alpha_l - 375.76 * alpha_l + 1471;
	const double p4 = 805.52E-3 * alpha_l + 1.8929 - 51.164E-3 * alpha_l * alpha_l;
	const double CyDeltaV = 2 * (p2 * 1E-12 * exp(mach) - p1 * 1E-6 * mach * mach - p3 * 1E-6 * mach - p4 * 1E-3);

	return CyDeltaV;
}

inline double Aerodynamics::CzBetta(const double mach) {
	return -CyAlpha(mach);
}

inline double Aerodynamics::CzDeltaN(const double mach, const double betta) {
	return  -CyDeltaV(mach, betta);
}

inline double Aerodynamics::mxOmegax() {
	return (-0.003393);     	//return (-0.005 * 0.6786);
}

inline double Aerodynamics::mxDeltaR(const double q, const double SmL) {
	return(-1000 / (q * SmL));
}

inline double Aerodynamics::mzOmegaz(const double mach) {
	return 1.89 * (146.79E-6 * mach * mach - 158.98E-3 / mach - 7.6396E-3 * mach - 68.195E-3);
}

inline double Aerodynamics::mzAlpha(const double mach) {
	return((-766.79E-3) / exp(mach) + 438.74E-3 / mach + 5.8822E-3 * mach - 158.34E-3);
}

inline double Aerodynamics::mzDeltaP(const double mach, const double alpha) {
	const double alpha_l = fabs(alpha * RAD_BY_DEGR);
	const double k1 = exp(-19.488E-3 * alpha_l * alpha_l - 378.62E-3 * alpha_l + 6.7518);
	const double k2 = exp(-21.234E-3 * alpha_l * alpha_l - 635.84E-6 * exp(alpha_l) - 98.296E-3 * alpha_l + 2.5938);
	const double mzDeltvh = (1.89 * sqrt(k1 * 1E-9 * mach * mach + k2 * 1E-6));
	return mzDeltvh;
}

inline double Aerodynamics::myBetta(const double mach) {
	return mzAlpha(mach);
}
inline double Aerodynamics::myOmegay(const double mach) {
	return mzOmegaz(mach);
}
inline double Aerodynamics::myDeltaY(const double mach, const double betta) {
	return mzDeltaP(mach, betta);
}


