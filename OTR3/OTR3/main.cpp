#include "Integration.h"
#include <thread>
#include <vector>
#include <mutex>

#include <iostream>
#include <fstream>
#include <cstdint>
#include <filesystem>

using namespace std;


namespace fs = std::filesystem;

//TODO ������� ��� ������ � ��������� ����
void mainMenu(); void mainMenuText();

void getFlightOTR(
	const int typeDelay, const int TdelayRange,
	double dh, double xTarget, double zTarget, string nameFile, long STEP0
);
void getAreaImpact(
	const int typeDelay, const int TdelayRange, double dh, 
	double X_TARGET_BEGIN0, double X_TARGET_END0, double Z_TARGET_END0,
	double X_STEP0, double Z_STEP0, string nameFile
);

/// MAIN 
int main() {

	mainMenu();

}

mutex mtx;

Integration integration;

int counterThread = 0;

void mainMenu() {

	setlocale(LC_ALL, "Rus");

	mainMenuText();

	string path = "C:/Users/psvmo/Desktop/result_OTR3";

	cout << "������� ���� � �������� ����� ��� ����������� ���������\n ������ ��� windows:" << path << endl << "path:"; cin >> path;

	fs::current_path(fs::temp_directory_path());
	fs::create_directory(path);
	fs::permissions(path, fs::perms::others_all, fs::perm_options::remove);

	for (;;) {

		// number task
		int number = 0;

		// integration step
		double dh = 0.0001;

		// step write to file
		long STEP0 = 1;

		// for 1 algoritm - imlementation delay
		int typeDelay = 0;
		int TdelayRange = 1;

		// � ������ ������ ����� ������ ��������� ��������� ���� � ���� �����������
	INVALID_DATA_ENTRY:

		cout << "\n\n============================================\n�������� ������� ������� �� ����. �� �����: ";
		cin >> number;

		cout << "Integration step dt = " << dh;
		cout << "\n��� �������� [0, 1, 2]: ";	cin >> typeDelay;

		if (typeDelay == 1 || typeDelay == 2) {

			double tDelay = 1;
			cout << "������� ��������, �������� tDelay = 0.1 [c]:\ntDelay = "; cin >> tDelay;
			TdelayRange = int(tDelay / dh);
			cout << "ArraySize = " << TdelayRange << endl;

		} else if (typeDelay != 0) {
			std::cout << "invalid data entry\n";
			goto INVALID_DATA_ENTRY;
		}

		if (std::cin.fail()) {
			std::cin.clear(); std::cin.ignore(32767, '\n');
			std::cout << "invalid data entry\n";
			goto INVALID_DATA_ENTRY;
		}

		switch (number) {
		case 1: {

			double xTarget = 5000, zTarget = 700;

			std::cout << "xTarget   = "; cin >> xTarget;
			std::cout << "zTarget   = "; cin >> zTarget;
			std::cout << "STEP0     = "; cin >> STEP0;

			string nameFile = path + "/Flight parameters typeDelay_" + to_string(typeDelay) + " xTarget_" + to_string(xTarget) + " zTarget_" + to_string(zTarget) + " STEP0_" + to_string(STEP0) + ".txt";

			++counterThread;

			thread th(getFlightOTR, typeDelay, TdelayRange, dh, xTarget, zTarget, nameFile, STEP0);
			th.detach();

			std::cout << endl << endl;
			break;
		}
		case 2: {

			double X_TARGET_BEGIN0 = 4000, X_TARGET_END0 = 7100;
			double Z_TARGET_END0 = 1500;
			double X_STEP0 = 10, Z_STEP0 = 10;
			string nameFile = path + "/";

			counterThread++;

			thread th(getAreaImpact, typeDelay, TdelayRange, dh, X_TARGET_BEGIN0, X_TARGET_END0, Z_TARGET_END0, X_STEP0, Z_STEP0, nameFile);
			th.detach();

			cout << endl << endl;
			break;
		}
		case 3: {
			
			double X_TARGET_BEGIN0 = 4000, X_TARGET_END0 = 7100;
			double Z_TARGET_END0 = 1500;
			double X_STEP0 = 10, Z_STEP0 = 10;
			string nameFile = "";

			counterThread++;

			//getAreaImpactExpedited(typeDelay, TdelayRange, dh, X_TARGET_BEGIN0, X_TARGET_END0, Z_TARGET_END0, X_STEP0, Z_STEP0, nameFile);
			
			cout << endl << endl;
			break;

		}
		default:
			std::cout << "\n\n!!! ������������ ����� ������ �� ���� !!!\n\n";

			mainMenu();
			goto INVALID_DATA_ENTRY;
			break;
		}
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

void getFlightOTR(const int typeDelay, const int TdelayRange, double dh, double xTarget, double zTarget, string nameFile, long STEP0) {

	std::cout << "number thread " << this_thread::get_id() << "\trunning threads: " << counterThread << "\n";
	std::cout << "The calculation started...\n";
	auto begin = std::chrono::high_resolution_clock::now();

	double miss = integration.flightSimple(typeDelay, TdelayRange, dh, xTarget, zTarget, nameFile, STEP0);

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> runtime = (end - begin); // � ���

	mtx.lock();

	counterThread--;

	std::cout << "\n\nCalculation completed. Name file: " + nameFile + "\n";
	std::cout << "\n\n                         runtime: " << runtime.count();
	std::cout << "\nnumber thread " << this_thread::get_id() << "\trunning threads: " << counterThread << "\n";

	mtx.unlock();
}


void getAreaImpact(const int typeDelay, const int TdelayRange, double dh, double X_TARGET_BEGIN0, double X_TARGET_END0, double Z_TARGET_END0, double X_STEP0, double Z_STEP0, string nameFile) {

	cout << "number thread " << this_thread::get_id() << "\trunning threads: " << counterThread << "\n";
	cout << "The calculation started...\n";

	auto begin = chrono::high_resolution_clock::now();

	vector<double> vecTargetX_0_50;		vector<double> vecTargetZ_0_50;
	vector<double> vecTargetX_0_5;		vector<double> vecTargetZ_0_5;
	vector<double> vecTargetX_5_30;		vector<double> vecTargetZ_5_30;
	vector<double> vecTargetX_30_50;	vector<double> vecTargetZ_30_50;
	vector<double> vecTargetX_45_55;	vector<double> vecTargetZ_45_55;
	//vector<double> vecTargetX_55_400;	vector<double> vecTargetZ_55_400;

	const double MISS[7] = { -0.00001, 5.1, 30.1, 50.1, 45.1, 55.1, 400 };

	// ������������ ��������� �����
	const double X_TARGET_BEGIN = X_TARGET_BEGIN0;
	const double X_TARGET_END = X_TARGET_END0;
	const double Z_TARGET_BEGIN = 0;
	const double Z_TARGET_END = Z_TARGET_END0;
	const double X_STEP = X_STEP0;
	const double Z_STEP = Z_STEP0;

	double xTarget = X_TARGET_BEGIN;
	double zTarget = Z_TARGET_BEGIN;

	double miss = 1;

	while (xTarget < X_TARGET_END) {

		miss = integration.flightForAreaImpact(typeDelay, TdelayRange, dh, xTarget, zTarget);

		if (MISS[0] < miss && miss <= MISS[3]) {
			vecTargetX_0_50.push_back(xTarget);
			vecTargetZ_0_50.push_back(zTarget);
		}

		if (MISS[0] < miss && miss <= MISS[1]) {
			vecTargetX_0_5.push_back(xTarget);
			vecTargetZ_0_5.push_back(zTarget);
			
			if (zTarget < Z_TARGET_END) {

				zTarget += Z_STEP;

			}
			else if (zTarget >= Z_TARGET_END - 1) {
				
				xTarget += X_STEP;
				zTarget = 0;
			}
		} else if (MISS[1] < miss && miss <= MISS[2]) {

			vecTargetX_5_30.push_back(xTarget);
			vecTargetZ_5_30.push_back(zTarget);
			
			if (zTarget < Z_TARGET_END) {
			
				zTarget += Z_STEP;

			} else if (zTarget >= Z_TARGET_END - 1) {

				xTarget += X_STEP;
				zTarget = 0;
			}
		} else if (MISS[2] < miss && miss <= MISS[3]) {
			
			vecTargetX_30_50.push_back(xTarget);
			vecTargetZ_30_50.push_back(zTarget);
			
			if (zTarget < Z_TARGET_END) {
				
				zTarget += Z_STEP;

			} else if (zTarget >= Z_TARGET_END - 1) {
			
				xTarget += X_STEP;
				zTarget = 0;

			}
		}

		if (MISS[4] < miss && miss <= MISS[5]) {
			
			vecTargetX_45_55.push_back(xTarget);
			vecTargetZ_45_55.push_back(zTarget);
			
			if (zTarget < Z_TARGET_END) {
			
				zTarget += Z_STEP;

			} else if (zTarget >= Z_TARGET_END - 1) {
				
				xTarget += X_STEP;
				zTarget = 0;

			}
		} else if (MISS[5] < miss && miss <= MISS[6]) {
			if (zTarget < Z_TARGET_END) {

				zTarget += 50;

			}
			else if (zTarget >= Z_TARGET_END - 1) {
				
				xTarget += X_STEP;
				zTarget = 0;

			}
		} else if (MISS[6] < miss) {
		
			xTarget += X_STEP;
			zTarget = 0;

		}
	}

	auto end = chrono::high_resolution_clock::now();
	chrono::duration<double> runtime = (end - begin);

	mtx.lock();

	string nameZona_0_50 = nameFile + to_string(typeDelay) + "__" + to_string(dh * TdelayRange) + "s__" + "_" + to_string(MISS[0]) + "_" + to_string(MISS[3]) + "_.txt";
	string nameZona_0_5 = nameFile + to_string(typeDelay) + "__" + to_string(dh * TdelayRange) + "s__" + "_" + to_string(MISS[0]) + "_" + to_string(MISS[1]) + "_.txt";
	string nameZona_5_30 = nameFile + to_string(typeDelay) + "__" + to_string(dh * TdelayRange) + "s__" + "_" + to_string(MISS[1]) + "_" + to_string(MISS[2]) + "_.txt";
	string nameZona_30_50 = nameFile + to_string(typeDelay) + "__" + to_string(dh * TdelayRange) + "s__" + "_" + to_string(MISS[2]) + "_" + to_string(MISS[3]) + "_.txt";
	string nameZona_45_55 = nameFile + to_string(typeDelay) + "__" + to_string(dh * TdelayRange) + "s__" + "_" + to_string(MISS[4]) + "_" + to_string(MISS[5]) + "_.txt";
	
	// ������ ����
	ofstream FileZona_0_50(nameZona_0_50, ios::out);
	for (int i = 0; i < vecTargetX_0_50.size(); i++) {
		FileZona_0_50 << vecTargetX_0_50[i] << "\t" << vecTargetZ_0_50[i] << "\t" << endl;
	}
	for (int i = vecTargetX_0_50.size() - 1; i > 0; i--) {
		FileZona_0_50 << vecTargetX_0_50[i] << "\t" << -vecTargetZ_0_50[i] << "\t" << endl;
	}
	FileZona_0_50.close();

	ofstream FileZona_0_5(nameZona_0_5, ios::out);
	for (int i = 0; i < vecTargetX_0_5.size(); i++) {
		FileZona_0_5 << vecTargetX_0_5[i] << "\t" << vecTargetZ_0_5[i] << "\t" << endl;
	}
	for (int i = vecTargetX_0_5.size() - 1; i > 0; i--) {
		FileZona_0_5 << vecTargetX_0_5[i] << "\t" << -vecTargetZ_0_5[i] << "\t" << endl;
	}
	FileZona_0_5.close();

	ofstream FileZona_5_30(nameZona_5_30, ios::out);
	for (int i = 0; i < vecTargetX_5_30.size(); i++) {
		FileZona_5_30 << vecTargetX_5_30[i] << "\t" << vecTargetZ_5_30[i] << "\t" << endl;
	}
	for (int i = vecTargetX_5_30.size() - 1; i > 0; i--) {
		FileZona_5_30 << vecTargetX_5_30[i] << "\t" << -vecTargetZ_5_30[i] << "\t" << endl;
	}
	FileZona_5_30.close();

	ofstream FileZona_30_50(nameZona_30_50, ios::out);
	for (int i = 0; i < vecTargetX_30_50.size(); i++) {
		FileZona_30_50 << vecTargetX_30_50[i] << "\t" << vecTargetZ_30_50[i] << "\t" << endl;
	}
	for (int i = vecTargetX_30_50.size() - 1; i > 0; i--) {
		FileZona_30_50 << vecTargetX_30_50[i] << "\t" << -vecTargetZ_30_50[i] << "\t" << endl;
	}
	FileZona_30_50.close();

	ofstream FileZona_45_55(nameZona_45_55, ios::out);
	for (int i = 0; i < vecTargetX_45_55.size(); i++) {
		FileZona_45_55 << vecTargetX_45_55[i] << "\t" << vecTargetZ_45_55[i] << "\t" << endl;
	}
	for (int i = vecTargetX_45_55.size() - 1; i > 0; i--) {
		FileZona_45_55 << vecTargetX_45_55[i] << "\t" << -vecTargetZ_45_55[i] << "\t" << endl;
	}
	FileZona_45_55.close();

	counterThread--;
	std::cout << "\n\nCalculation completed. tDelay: " << TdelayRange * dh;
	std::cout << "\n\n                      runtime: " << runtime.count();
	std::cout << "\nnumber thread " << this_thread::get_id() << "\trunning threads: " << counterThread << "\n";

	mtx.unlock();

}

void mainMenuText() {
	std::cout << "\n\n\t\tMENU\n"
		<< "1. ������ ���������� ���������� ������ ��� � ����� ����\n"
		<< "2. ������ ���� ��������� ���\n"
		//<< "3. ������ ���� ��������� ��� (����������)\n"
		;
}
