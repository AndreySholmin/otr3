#pragma once
#include "Vehicle.h"
#include "Aerodynamics.h"
#include "Control.h"
#include "Atmosphere.h"

class OTR {
private:
	int stepControl = NULL;

public:

	Vehicle vh;
	Aerodynamics ad;
	Control cs;

	void updateParameters(Atmosphere& atm);

};