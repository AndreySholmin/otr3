#pragma once

#include <cstddef>
#include <cmath>
#include "const.h"

class Vehicle {

public:
	// ��������� ���
	double dm = 0.95;        const double Ix = 170;
	double LENGTH = 7;       const double Iy = 640;
	double M = 1450;         const double Iz = 640;

	const double IzIy_Ix = (Iz - Iy) / Ix;
	const double IxIz_Iy = (Ix - Iz) / Iy;
	const double IyIx_Iz = (Iy - Ix) / Iz;

	// ������� ������
	const double Sm = dm * dm * M_PI_4;

	const double SmL = Sm * LENGTH;

	double t = NULL;

	double r = 0.0;

	double Vx = 1200;   double Vxg = NULL;  double Vr = NULL;
	double Vy = NULL;   double Vyg = NULL;	double Vfi = NULL;
	double Vz = NULL;   double Vzg = NULL;	double Vhi = NULL;

	double Wx = NULL;
	double Wy = NULL;
	double Wz = NULL;

	double velocity = NULL;

	double x = NULL;   double omegax = NULL;    double pitch = -52 * DEGR_BY_RAD;
	double y = 7500;   double omegay = NULL;    double yaw = NULL;
	double z = NULL;   double omegaz = NULL;    double roll = NULL;

	double derPitch = NULL;
	double derYaw = NULL;
	double derRoll = NULL;

	double q = NULL;       double alpha = NULL;    double derFi = NULL;
	double mach = NULL;    double betta = NULL;    double derHi = NULL;


	double qSm = NULL;
	double qSmL = NULL;
	double qSmLL_v = NULL;


	// target coordinates
	double xtarget = NULL;
	double ytarget = NULL;
	double ztarget = NULL;

	double deltaPitch = NULL;
	double deltaYaw = NULL;
	double deltaRoll = NULL;

	// force and moment base
	double X = NULL, Y = NULL, Z = NULL, Mx = NULL, My = NULL, Mz = NULL;

	// auxiluary force and moment 
	double Y_alpha = NULL, Y_delta_p = NULL, Z_beta = NULL, Z_delta_y = NULL;
	double Mx_omgx = NULL, Mx_delta_r = NULL,
		My_omgy = NULL, My_beta = NULL, My_delta_y = NULL,
		Mz_omgz = NULL, Mz_alpha = NULL, Mz_delta_p = NULL;


	void updateParameters(const double density, const double soundVelocity);

};