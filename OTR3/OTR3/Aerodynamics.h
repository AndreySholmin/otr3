#pragma once
#include "Vehicle.h"
#include "const.h"


class Aerodynamics {
private:
	inline double  /// ������������
		Cx(const double mach), CyAlpha(const double mach),
		CyDeltaV(const double mach, const double alpha),
		CzBetta(const double mach), CzDeltaN(const double mach, const double betta),
		mxOmegax(), mxDeltaR(const double q, const double SmL),
		mzOmegaz(const double mach), mzAlpha(const double mach), mzDeltaP(const double mach, const double alpha),
		myBetta(const double mach), myOmegay(const double mach), myDeltaY(const double mach, const double betta);
public:
	void updateParameters(Vehicle& vh);

};

