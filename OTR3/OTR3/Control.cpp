#include "Control.h"

/// <summary>
///				��� ��������� �� ������� 
///	 Deltvhitch =  Kpitch1 * derFi   - Kpitch2 * derPitch
///	 DeltaYaw   =  Kyaw1   * derHi   - Kyaw2   * derYaw
///	 DeltaRoll  = -Kroll1  * derRoll - Kroll2  * Roll
/// 
///			source expression control system
/// b42 = Z_beta / M / velocity -- ERROR: a big miss 
/// K_r = c13 / c11             -- ERROR: a big miss 
/// Source data - error expression 
/// double a11 = -Mz_omgz * LENGTH / Iz /velocity;
/// double b11 = -My_omgy * LENGTH / Iy / velocity;
/// double c11 = -Mx_omgx * LENGTH / Ix / velocity;
/// </summary>

void Control::updateParameters(Vehicle& vh) {

	const double a11 = -vh.Mz_omgz / vh.Iz;
	const double a12 = -vh.Mz_alpha / vh.Iz;
	const double a13 = -vh.Mz_delta_p / vh.Iz;
	const double a42 = vh.Y_alpha / vh.M / vh.velocity;
	const double a43 = vh.Y_delta_p / vh.M / vh.velocity;

	const double b11 = -vh.My_omgy / vh.Iy;
	const double b12 = -vh.My_beta / vh.Iy;
	const double b13 = -vh.My_delta_y / vh.Iy;
	const double b42 = -vh.Z_beta / vh.M / vh.velocity;
	const double b43 = -vh.Z_delta_y / vh.M / vh.velocity;

	const double c11 = -vh.Mx_omgx / vh.Ix;
	const double c13 = -vh.Mx_delta_r / vh.Ix;

	const double K_p = (a12 * a43 - a13 * a42) / (a12 + a11 * a42);
	const double T1_p = -a13 / (a13 * a42 - a12 * a43);
	const double T_p = 1 / sqrt(a12 + a11 + a42);
	const double E_p = (a11 + a42) / 2 / sqrt(a12 + a11 * a42);

	const double K_y = (b12 * b43 - b13 * b42) / (b12 + b11 * b42);
	const double T1_y = -b13 / (b13 * b42 - b12 * b43);
	const double T_y = 1 / sqrt(b12 + b11 + b42);
	const double E_y = (b11 + b42) / 2 / sqrt(b12 + b11 * b42);

	const double K_r = -c13 / c11;
	const double T_r = 1 / c11;

	const double K1_p = Kcc_p * (1 + Ky_p * Krp_p * Kdg_p * K_p) / K_p / T1_p / T1_p;
	const double K2_p = -2 * T_p * (E_p * T1_p - Ecc_p * Ecc_p * T_p - sqrt(Ecc_p * Ecc_p * Ecc_p * Ecc_p * T_p * T_p - 2 * E_p * Ecc_p * Ecc_p * T_p * T1_p + T1_p * T1_p * Ecc_p * Ecc_p)) / K_p / T1_p / T1_p;

	const double K1_y = Kcc_y * (1 + Ky_y * Krp_y * Kdg_y * K_y) / K_y / T1_y / T1_y;
	const double K2_y = -2 * T_y * (E_y * T1_y - Ecc_y * Ecc_y * T_y - sqrt(Ecc_y * Ecc_y * Ecc_y * Ecc_y * T_y * T_y - 2 * E_y * Ecc_y * Ecc_y * T_y * T1_y + T1_y * T1_y * Ecc_y * Ecc_y)) / K_y / T1_y / T1_y;

	const double K1_r = (2 * Ecc_r * T_r - Tcc_r) / K_r / Tcc_r;
	const double K2_r = T_r / K_r / Tcc_r / Tcc_r;

	const double fi = asin((vh.ytarget - vh.y) / vh.r);
	const double hi = -atan2((vh.ztarget - vh.z), (vh.xtarget - vh.x));

	// transition matrix
	double b[3][3];
	b[0][0] = cos(fi) * cos(hi);	b[0][1] = sin(fi);	b[0][2] = -cos(fi) * sin(hi);
	b[1][0] = -sin(fi) * cos(hi);	b[1][1] = cos(fi);	b[1][2] = sin(fi) * sin(hi);
	b[2][0] = sin(hi);	            b[2][1] = 0;       	b[2][2] = cos(hi);

	vh.Vr = -(b[0][0] * vh.Vxg + b[0][1] * vh.Vyg + b[0][2] * vh.Vzg);
	vh.Vfi = -(b[1][0] * vh.Vxg + b[1][1] * vh.Vyg + b[1][2] * vh.Vzg);
	vh.Vhi = -(b[2][0] * vh.Vxg + b[2][1] * vh.Vyg + b[2][2] * vh.Vzg);

	/// angle derivatives
	vh.derPitch = vh.omegay * sin(vh.roll) + vh.omegaz * cos(vh.roll);
	vh.derYaw = (vh.omegay * cos(vh.roll) - vh.omegaz * sin(vh.roll)) / cos(vh.pitch);
	vh.derRoll = vh.omegax - tan(vh.pitch) * (vh.omegay * cos(vh.roll) - vh.omegaz * sin(vh.roll));

	vh.derFi = vh.Vfi / vh.r;
	vh.derHi = (-vh.Vhi) / (vh.r * cos(fi));

	const double Kp1 = K1_p * Kkp * Kdfi;
	const double Kp2 = -K2_p;
	const double Ky1 = K1_y * Kky * Kdhi;
	const double Ky2 = -K2_y;
	const double Kr1 = -K1_r;
	const double Kr2 = -K2_r;

	/// the calculation of the equivalent deflection angles of the aerodynamic rudders
	switch (typeDelay) {
		case 0: {
			vh.deltaPitch = Kp1 * vh.derFi + Kp2 * vh.derPitch;
			vh.deltaYaw = Ky1 * vh.derHi + Ky2 * vh.derYaw;
			vh.deltaRoll = Kr1 * vh.derRoll + Kr2 * vh.roll;

			break;
		}
		case 1: {
			const int indexDelay = TdelayRange - indexTemp;

			vh.deltaPitch = Kp1 * vh.derFi + Kp2 * vh.derPitch;
			vh.deltaYaw = Ky1 * vh.derHi + Ky2 * vh.derYaw;
			vh.deltaRoll = Kr1 * vh.derRoll + Kr2 * vh.roll;

			d1 = deltaPitchArray[indexDelay];
			d2 = deltaYawArray[indexDelay];
			d3 = deltaRollArray[indexDelay];

			deltaPitchArray[indexDelay] = vh.deltaPitch;
			deltaYawArray[indexDelay] = vh.deltaYaw;
			deltaRollArray[indexDelay] = vh.deltaRoll;

			vh.deltaPitch = d1;
			vh.deltaYaw = d2;
			vh.deltaRoll = d3;

			indexTemp--;

			if (indexTemp == 0) {
				indexTemp = TdelayRange;
			}

			break;
		}
		case 2: {
			vh.deltaPitch = d1;
			vh.deltaYaw = d2;
			vh.deltaRoll = d3;

			d1 = Kp1 * vh.derFi + Kp2 * vh.derPitch;
			d2 = Ky1 * vh.derHi + Ky2 * vh.derYaw;
			d3 = Kr1 * vh.derRoll + Kr2 * vh.roll;

			break;
		}
		default: {
			std::cout << "Error typeDelay";
			std::system("pause");
			break;
		}
	}

	// ����������� ����� ���������� �����
	if (vh.deltaPitch > �riticalDeltaAngle)		
		vh.deltaPitch = �riticalDeltaAngle;
	if (vh.deltaPitch < -�riticalDeltaAngle)	
		vh.deltaPitch = -�riticalDeltaAngle;

	if (vh.deltaYaw > �riticalDeltaAngle)		
		vh.deltaYaw = �riticalDeltaAngle;
	if (vh.deltaYaw < -�riticalDeltaAngle)		
		vh.deltaYaw = -�riticalDeltaAngle;

	if (vh.deltaRoll < -�riticalDeltaAngle)		
		vh.deltaRoll = -�riticalDeltaAngle;
	if (vh.deltaRoll > �riticalDeltaAngle)
		vh.deltaRoll = �riticalDeltaAngle;

}

void Control::setTypeDelay(int typeDelay) {
	this->typeDelay = typeDelay;
}
// TODO ������� ��������� ������������ �������� ��� ���� ��� ��������
void Control::setTdelayRange(int TdelayRange) {
	this->TdelayRange = TdelayRange;
	this->indexTemp = TdelayRange;

	deltaPitchArray = new double[TdelayRange] {NULL};
	deltaYawArray = new double[TdelayRange] {NULL};
	deltaRollArray = new double[TdelayRange] {NULL};
}

int Control::getTypeDelay() {
	return typeDelay;
}

int Control::getTdelayRange() {
	return TdelayRange;
}

Control::~Control() {
	delete deltaPitchArray;
	delete deltaYawArray;
	delete deltaRollArray;
}
