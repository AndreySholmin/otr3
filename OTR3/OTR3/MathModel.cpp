#include "MathModel.h"

// TODO �������� ��� ���������
double MathModel::calcGetPitch() {
	return asin(2 * (RGro * RGny + RGly * RGmy));
}

// TODO �������� ��� ���������
double MathModel::calcGetYaw() {
	return atan2((2 * (RGro * RGmy - RGly * RGny)), (RGro * RGro + RGly * RGly - RGmy * RGmy - RGny * RGny));
}

// TODO �������� ��� ���������
double MathModel::calcGetRoll() {
	return atan2((2 * (RGro * RGly - RGny * RGmy)), (RGro * RGro + RGmy * RGmy - RGny * RGny - RGly * RGly));
}

// �� �� �� - > ���� ��
void MathModel::setMatrixRGParameters() {
	/// ������������ ��������� ��
	double VectorRG = sqrt(RGro * RGro + RGly * RGly + RGmy * RGmy + RGny * RGny);

	RGro /= VectorRG;
	RGly /= VectorRG;
	RGmy /= VectorRG;
	RGny /= VectorRG;

	a[0][0] = RGro * RGro + RGly * RGly - RGmy * RGmy - RGny * RGny;
	a[0][1] = 2 * (RGro * RGny + RGly * RGmy);
	a[0][2] = 2 * (RGly * RGny - RGro * RGmy);

	a[1][0] = 2 * (RGly * RGmy - RGro * RGny);
	a[1][1] = RGro * RGro - RGly * RGly + RGmy * RGmy - RGny * RGny;
	a[1][2] = 2 * (RGro * RGly + RGny * RGmy);

	a[2][0] = 2 * (RGro * RGmy + RGly * RGny);
	a[2][1] = 2 * (RGny * RGmy - RGro * RGly);
	a[2][2] = RGro * RGro + RGny * RGny - RGmy * RGmy - RGly * RGly;
}

void MathModel::updateDerivatives(Vehicle& la) {
	// Gravity
	Gy = -mu * la.M / ((R + la.y) * (R + la.y));
	//
	dRGro = -0.5 * (la.omegax * RGly + la.omegay * RGmy + la.omegaz * RGny);
	dRGly = 0.5 * (la.omegax * RGro - la.omegay * RGny + la.omegaz * RGmy);
	dRGmy = 0.5 * (la.omegax * RGny + la.omegay * RGro - la.omegaz * RGly);
	dRGny = 0.5 * (-la.omegax * RGmy + la.omegay * RGly + la.omegaz * RGro);
	//
	dOmegaX = la.Mx / la.Ix - la.IzIy_Ix * la.omegay * la.omegaz;
	dOmegaY = la.My / la.Iy - la.IxIz_Iy * la.omegax * la.omegaz;
	dOmegaZ = la.Mz / la.Iz - la.IyIx_Iz * la.omegax * la.omegay;

	dVxg = (la.X * a[0][0] + la.Y * a[1][0] + la.Z * a[2][0]) / la.M;
	dVyg = (la.X * a[0][1] + la.Y * a[1][1] + la.Z * a[2][1] + Gy) / la.M;
	dVzg = (la.X * a[0][2] + la.Y * a[1][2] + la.Z * a[2][2]) / la.M;
	//
	dX = la.Vxg;
	dY = la.Vyg;
	dZ = la.Vzg;
}

// for first step
void MathModel::setRGParameters(double pitch, double yaw, double roll) {

	RGro = cos(0.5 * yaw) * cos(0.5 * pitch) * cos(0.5 * roll) - sin(0.5 * yaw) * sin(0.5 * pitch) * sin(0.5 * roll);
	RGly = sin(0.5 * yaw) * sin(0.5 * pitch) * cos(0.5 * roll) - cos(0.5 * yaw) * cos(0.5 * pitch) * sin(0.5 * roll);
	RGmy = sin(0.5 * yaw) * cos(0.5 * pitch) * cos(0.5 * roll) - cos(0.5 * yaw) * sin(0.5 * pitch) * sin(0.5 * roll);
	RGny = cos(0.5 * yaw) * sin(0.5 * pitch) * cos(0.5 * roll) - sin(0.5 * yaw) * cos(0.5 * pitch) * sin(0.5 * roll);
}
