#include "OTR.h"

void OTR::updateParameters(Atmosphere& atm) {

	vh.updateParameters(atm.getDensity(), atm.getSoundVelocity());
	ad.updateParameters(vh);

	// control
	if (vh.r >= 500) {
		if (cs.typeDelay == 0 || cs.typeDelay == 1) {
			
			cs.updateParameters(vh);

		} else if (cs.typeDelay == 2) {
			if (stepControl % cs.TdelayRange == 0) {
				
				stepControl = 0;
				cs.updateParameters(vh);

			}
			stepControl++;
		}
	}

}