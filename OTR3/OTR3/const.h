#pragma once
#include <corecrt_math_defines.h>

const double RAD_BY_DEGR = 180 / M_PI;
const double DEGR_BY_RAD = M_PI / 180;